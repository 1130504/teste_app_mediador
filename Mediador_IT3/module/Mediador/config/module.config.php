<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Mediador\Controller\Mediador' => 'Mediador\Controller\MediadorController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'mediador' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/mediador[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'        => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Mediador\Controller\Mediador',
                                'action' => 'index',
                            ),
                        ),
                    ),
                ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Mediador' => __DIR__ . '/../view',
        ),
    ),
);

