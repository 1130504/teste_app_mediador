<?php
namespace Mediador\DTO;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class Correspondencia{
    
    public $correspondencia;
    public $anuncio_venda;
    public $anuncio_compra;
    
    public $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->$correspondencia     = (!empty($data['$correspondencia'])) ? $data['$correspondencia'] : null;
        $this->$anuncio_venda     = (!empty($data['$anuncio_venda'])) ? $data['$anuncio_venda'] : null;
        $this->$anuncio_compra     = (!empty($data['$$anuncio_compra'])) ? $data['$$anuncio_compra'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    public function getInputFilter(){
        throw new \Exception("Not used");
    }
}
?>