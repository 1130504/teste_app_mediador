<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Mediador for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Mediador\Controller;

use Mediador\Form\anuncioCompra;
use Mediador\Form\anuncioPermuta;
use Mediador\Form\anuncioAluguer;
use Mediador\Form\anuncioVenda;
use Mediador\Form\LoginForm;
use Mediador\Form\Credenciais;
use Mediador\Services\ImoServices;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;
use Zend\Debug\Debug;
use Mediador\Form\RegisterForm;
use Mediador\Form\removeraluguer;
use Mediador\Form\removercompra;
use Mediador\Form\removerpermuta;
use Mediador\Form\removervenda;
use Mediador\Form\corresponderCompra;
use Mediador\DTO\Correspondencia;

class MediadorController extends AbstractActionController
{
    public function indexAction()
    {
        $num1 = ImoServices::getAllAnuncio("Anuncio_Venda");
        $num2 = ImoServices::getAllAnuncio("Anuncio_Compra");
        $num3 = ImoServices::getAllAnuncio("Anuncio_Aluguer");
        $num4 = ImoServices::getAllAnuncio("Anuncio_Permuta");
        
        return new ViewModel(array('venda' => $num1,'compra' => $num2,'aluguer' => $num3,'permuta' => $num4));
    }
    
    

    public function corresponderAction()
    {   
      $arrayCorrespondenciasDTO = array();
      $correspondencias = ImoServices::getAllCorrespondencias();
      
      if(!is_null($correspondencias)){
        foreach ($correspondencias as $correspondencia){
            // buscar id's dos anuncios
            $idVenda = $correspondencia['Anuncio_venda_ID'];
            $idCompra = $correspondencia['Anuncio_compra_ID'];
            //buscar anuncios
            $anuncioVenda = ImoServices::getAnuncio("Anuncio_Venda", $idVenda);
            $anuncioCompra = ImoServices::getAnuncio("Anuncio_Compra", $idCompra);
            
            if($anuncioVenda['valido'] == "true" && $anuncioCompra['valido'] == "true"){
                
                // cria DTO
                $correspondenciaDTO = new Correspondencia();
                
                //colocar no array de correspondenciasDTO
                $correspondenciaDTO ->correspondencia = $correspondencia;
                $correspondenciaDTO ->anuncio_compra = $anuncioCompra;
                $correspondenciaDTO ->anuncio_venda = $anuncioVenda;
                array_push($arrayCorrespondenciasDTO, $correspondenciaDTO);
            }            
        }
      }      
      $form = new corresponderCompra();
      $form->get('submit')->setValue('Associar');
      
      $request = $this->getRequest();
    
     if ($request->isPost()) {
         
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
                
                array_pop($var);
                
                ImoServices::associarVendaCompra($var['CorrespondenciaID']);
                
                return $this->redirect()->toRoute('mediador');
            }
        }
        return new ViewModel(array('form'=>$form,'correspondencias' =>$arrayCorrespondenciasDTO));
    }
    
    public function marcarVisitas(){
        $pedidos=ImoServices::getPedidos();
        return new ViewModel(array('pedidos'=>$pedidos));
    }
    
    public function agendar(){
        
        
    }
    
    
    public function confirmasAction()
    {
        return new ViewModel();
    }
    
    public function vendaAction()
    {
        $id = (int) $this->params()->fromRoute('id',0);
        
        $form = new anuncioVenda();
        $form->get('submit')->setValue('Confirma');
        
        $num = $this->getAnuncio("Anuncio_Venda", $id);
        
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function editvendaAction()
    {   
        $form = new anuncioVenda();
        $form->get('submit')->setValue('Edit');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
                array_pop($var);
               
                if($var['checkbox'] == 'true'){
                    $var['fotoContent'] = ImoServices::get3DImages($var['ImovelID']);
                }
                array_pop($var);
                ImoServices::editAnuncio($id, "Anuncio_Venda", $var);
                
                return $this->redirect()->toRoute('mediador');
            }
        }
        
        $num = ImoServices::getAnuncio("Anuncio_Venda", $id);
        $id_imovel = $num['ImovelID'];
        $lista_id_imoveis = ImoServices::get3DImagesID();
        $pos = strpos($lista_id_imoveis, strval($id_imovel));
        $data_imagem;
        if ($pos === false) {
            $data_imagem = "false";
        } else {
            $data_imagem = ImoServices::get3DImages($id_imovel);
        }
        return new ViewModel(array(
            'vec' => $num,'form' => $form,'Imagem' =>$data_imagem));
    }
    
    public function validavendaAction()
    {
    
        $form = new removervenda();
        $form->get('submit')->setValue('Valida');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
    
        if ($request->isPost()) {
    
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $var = $form->getData();
    
                array_pop($var);
    
                ImoServices::validaAnuncio($id, "Anuncio_Venda", $var);
    
    
                return $this->redirect()->toRoute('mediador');
            }
        }
    
         
    
        $num = ImoServices::getAnuncio("Anuncio_Venda", $id);
    
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    
    public function removevendaAction()
    {
        $form = new removervenda();
        $form->get('submit')->setValue('Remover');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
        
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
        
                ImoServices::removeAnuncio($id, "Anuncio_Venda");
        
                return $this->redirect()->toRoute('mediador');
            }
        }
        $num = ImoServices::getAnuncio("Anuncio_Venda", $id);
        
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function aluguerAction()
    {
        $id = (int) $this->params()->fromRoute('id',0);
        
        $form = new anuncioAluguer();
        $form->get('submit')->setValue('Confirma');
    
        $num = $this->getAnuncio("Anuncio_Aluguer", $id);
        
        
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function duasopcoesAction()
    {
        return new ViewModel();
    }
    
    public function editaluguerAction()
    {
        $form = new anuncioAluguer();
        $form->get('submit')->setValue('Edit');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
                
                array_pop($var);
                if($var['checkbox'] == 'true'){
                    $var['fotoContent'] = ImoServices::get3DImages($var['ImovelID']);
                }
                array_pop($var);
                ImoServices::editAnuncio($id, "Anuncio_Aluguer", $var);
                
                
                return $this->redirect()->toRoute('mediador');
            }
        }
        
        $num = ImoServices::getAnuncio("Anuncio_Aluguer", $id);
        $id_imovel = $num['ImovelID'];
        $lista_id_imoveis = ImoServices::get3DImagesID();
        $pos = strpos($lista_id_imoveis, strval($id_imovel));
        $data_imagem;
        if ($pos === false) {
            $data_imagem = "false";
        } else {
            $data_imagem = ImoServices::get3DImages($id_imovel);
        }
        return new ViewModel(array(
            'vec' => $num,'form' => $form,'Imagem' =>$data_imagem));

    }
    
    public function validaaluguerAction()
    {
    
        $form = new removeraluguer();
        $form->get('submit')->setValue('Valida');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
    
        if ($request->isPost()) {
    
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $var = $form->getData();
    
                array_pop($var);
    
                ImoServices::validaAnuncio($id, "Anuncio_Aluguer", $var);
    
    
                return $this->redirect()->toRoute('mediador');
            }
        }
    
         
    
        $num = ImoServices::getAnuncio("Anuncio_Aluguer", $id);
    
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function removealuguerAction()
    {
    
        $form = new removeraluguer();
        $form->get('submit')->setValue('Remover');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();

                ImoServices::removeAnuncio($id, "Anuncio_Aluguer");
                
                return $this->redirect()->toRoute('mediador');
            }
        }
        $num = ImoServices::getAnuncio("Anuncio_Aluguer", $id);
    
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    
    public function permutaAction()
    {
        $id = (int) $this->params()->fromRoute('id',0);
        
        $form = new anuncioPermuta();
        $form->get('submit')->setValue('Confirma');
    
        $num = $this->getAnuncio("Anuncio_Permuta", $id);
    
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }

    public function editpermutaAction()
    {
    
        $form = new anuncioPermuta();
        $form->get('submit')->setValue('Edit');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
                
                array_pop($var);
                if($var['checkbox'] == 'true'){
                    $var['fotoContent'] = ImoServices::get3DImages($var['ImovelID']);
                }
                array_pop($var);
                $this->editAnuncio($id,"Anuncio_Permuta", $var);
                ImoServices::editAnuncio($id, "Anuncio_Permuta", $var);
                
                
                return $this->redirect()->toRoute('mediador');
            }
        }
        
       
        
        $num = ImoServices::getAnuncio("Anuncio_Permuta", $id);
    
        $id_imovel = $num['ImovelID'];
        $lista_id_imoveis = ImoServices::get3DImagesID();
        $pos = strpos($lista_id_imoveis, strval($id_imovel));
        $data_imagem;
        if ($pos === false) {
            $data_imagem = "false";
        } else {
            $data_imagem = ImoServices::get3DImages($id_imovel);
        }
        return new ViewModel(array(
            'vec' => $num,'form' => $form,'Imagem' =>$data_imagem));
    }
    
    public function validapermutaAction()
    {
    
        $form = new removerpermuta();
        $form->get('submit')->setValue('Valida');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
    
        if ($request->isPost()) {
    
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $var = $form->getData();
    
                array_pop($var);
    
                ImoServices::validaAnuncio($id, "Anuncio_Permuta", $var);
    
    
                return $this->redirect()->toRoute('mediador');
            }
        }
    
         
    
        $num = ImoServices::getAnuncio("Anuncio_Permuta", $id);
    
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function removepermutaAction()
    {
        $form = new removerpermuta();
        $form->get('submit')->setValue('Remover');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
        
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
        
                ImoServices::removeAnuncio($id, "Anuncio_Permuta");
        
                return $this->redirect()->toRoute('mediador');
            }
        }
        $num = ImoServices::getAnuncio("Anuncio_Permuta", $id);
        
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    
    public function compraAction()
    {
        $id = (int) $this->params()->fromRoute('id',0);
        
        $form = new anuncioCompra();
        $form->get('submit')->setValue('Confirma');
        $num = $this->getAnuncio("Anuncio_Compra", $id);
        
        return new ViewModel(array(
            'vec' => $num,'form' => $form,'foto' => $byte_array));
    }
    
    public function editcompraAction()
    {
    
        $form = new anuncioCompra();
        $form->get('submit')->setValue('Edit');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
                
                array_pop($var);
                
                $this->editAnuncio($id,"Anuncio_Compra", $var);
                ImoServices::editAnuncio($id, "Anuncio_Compra", $var);
                
                
                return $this->redirect()->toRoute('mediador');
            }
        }
        
       
        
        $num = ImoServices::getAnuncio("Anuncio_Compra", $id);
    
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function validacompraAction()
    {
    
        $form = new removercompra();
        $form->get('submit')->setValue('Valida');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
    
        if ($request->isPost()) {
    
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $var = $form->getData();
    
                array_pop($var);
    
                ImoServices::validaAnuncio($id, "Anuncio_Compra", $var);
    
    
                return $this->redirect()->toRoute('mediador');
            }
        }
    
         
    
        $num = ImoServices::getAnuncio("Anuncio_Compra", $id);
    
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function removecompraAction()
    {
        $form = new removercompra();
        $form->get('submit')->setValue('Remover');
        $id = (int) $this->params()->fromRoute('id',0);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
        
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
        
                ImoServices::removeAnuncio($id, "Anuncio_Compra");
        
                return $this->redirect()->toRoute('mediador');
            }
        }
        $num = ImoServices::getAnuncio("Anuncio_Compra", $id);
        
        return new ViewModel(array(
            'vec' => $num,'form' => $form));
    }
    
    public function getValues($anuncio)
    {
        session_start();
        $client = new Client("https://localhost:44301/api/" . $anuncio);
        $client->setMethod(Request::METHOD_GET);
    
        
        $bearerToken = 'Bearer' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearerToken,
        ));
       
    
        $client->setOptions(['sslverifypeer' => false]);
    
        $response = $client->send();
    
        $body=Json::decode($response->getBody(), true);
    
        return $body;
    
    }
    
    public function getAnuncio($anuncio,$id)
    {
        session_start();
        $client = new Client("https://localhost:44301/api/" . $anuncio . "/" . $id);
        $client->setMethod(Request::METHOD_GET);
    
        
         $bearerToken = 'Bearer' . $_SESSION['access_token'];
         $client->setHeaders(array(
         'Authorization' => $bearerToken,
         ));
        
    
        $client->setOptions(['sslverifypeer' => false]);
    
        $response = $client->send();
    
        $body=Json::decode($response->getBody(), true);
    
    
        return $body;
    
    }
    public function loginAction()
    {
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
    
        $request = $this->getRequest();
        if ($request->isPost()) {
    
            session_start();
            ImoServices::Logout();
            $_SESSION['server'] = 'localhost:44301';
            $credenciais = new Credenciais();
            $form->setInputFilter($credenciais->getInputFilter());
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $credenciais->exchangeArray($form->getData());
                 
                if(ImoServices::Login($credenciais) )
    
                    // Redirect to values
                    return $this->redirect()->toRoute('mediador', array('action' => 'duasopcoes'));
            }
        }
        return array('form' => $form);
    }
    
    public function registerAction()
    {
        $form = new RegisterForm();
        $form->get('submit')->setValue('Register');
    
        $request = $this->getRequest();
        
        if ($request->isPost()) {
        
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $var = $form->getData();
        
                //Debug::dump($var['username']);
                //Debug::dump($var['password']);
                //return null;
        
               return ImoServices::Register($var['username'],$var['password']);
                
        
                return $this->redirect()->toRoute('mediador');
            }
        }
        return array('form' => $form);
    }
    
    
    public function logoutAction()
    {
        ImoServices::Logout();
    
        return $this->redirect()->toRoute('mediador', array('action' => 'login'));
    }
    
    public function valuesAction()
    {
        $body = ImoServices::getValues();
    
        return new ViewModel(array(
            'values' => Json::decode($body)
        ));
    }
    
    public function editAnuncio($id, $tipo, $data){
        session_start();
        $client = new Client('https://' .$_SESSION['server'].'/api/'.$tipo.'/'.$id, array());
    
        $client->setMethod(Request::METHOD_PUT);
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
        ));
    
        $bearerToken = 'Bearer' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearerToken,
        ));
        
        $encoded = Json::encode($data);
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($encoded);
    
        $response = $client->send();
        Debug::dump($response);
        return null;
    
        return true;
    }
}