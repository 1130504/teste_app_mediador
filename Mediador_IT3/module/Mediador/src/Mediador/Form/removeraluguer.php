<?php
namespace Mediador\Form;

use Zend\Form\Form;

class removeraluguer extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('aluguer');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'applicationUserID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'ImovelID',
            'type' => 'Hidden',
            ));
        
        $this->add(array(
            'name' => 'renda',
            'type' => 'Text',
            'options' => array(
                'label' => 'Renda: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'tipoImovelID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'localizacaoID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'fotoID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'tipoImovel',
            'type' => 'Text',
            'options' => array(
                'label' => 'Tipo de Imovel: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'local',
            'type' => 'Text',
            'options' => array(
                'label' => 'Local: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'latitude',
            'type' => 'Text',
            'options' => array(
                'label' => 'Latitude: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'longitude',
            'type' => 'Text',
            'options' => array(
                'label' => 'Longitude: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'area',
            'type' => 'Text',
            'options' => array(
                'label' => 'Area: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'fotoContent',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'fotoContentType',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'valido',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'MediadorID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
?>