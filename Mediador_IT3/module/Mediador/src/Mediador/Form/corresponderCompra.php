<?php
namespace Mediador\Form;

use Zend\Form\Form;

class corresponderCompra extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('compra');
        
        $this->add(array(
            'name' => 'CorrespondenciaID',
            'type' => 'Text',
            'options' => array(
                'label' => 'Aceitar/Rejeitar Correspondencia: '),
            ));
        
        $this->add(array(
            'name' => 'AnuncioCompraID',
            'type' => 'Text',
            'options' => array(
                'label' => 'Anuncio Compra: '),
        ));
        
    
        $this->add(array(
            'name' => 'AnuncioVendaID',
            'type' => 'Text',
            'options' => array(
                'label' => 'Anuncio Venda: '),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Corresponder',
                'id' => 'submitbutton',
            ),
        ));
    }
}
?>