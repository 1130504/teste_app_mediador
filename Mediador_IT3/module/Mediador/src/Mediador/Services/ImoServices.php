<?php
namespace Mediador\Services;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;
use Zend\Debug\Debug;

use Mediador\Form\Credenciais;

class ImoServices
{
    const URL_Login = '/Token';
    const URL_Values = '/api/values';
    const URL_Correspondencias = 'api/Correspondencia';
    
    public static function Login(Credenciais $credenciais)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'] .self::URL_Login);

        $client->setMethod(Request::METHOD_POST);
        $params = 'grant_type=password&username=' . $credenciais->username .'&password=' .$credenciais->password;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len 
        ));        
        
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
        
        $response = $client->send();            
        
        $body=Json::decode($response->getBody());
        
        
        
        if(!empty($body->access_token))
        {
        if(!isset($_SESSION)){
                        session_start();
                    }

            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['username'] = $credenciais->username;
            return true;
            
           /* $roles = (array)ImoServices::getRole();*/
            
            /*if (in_array("[\"Mediador\"]", $roles) || in_array("[\"Admin\"]", $roles)) { 
                return true;
            } else {
                $_SESSION['access_token'] = null;
                $_SESSION['username'] = null;
                session_destroy();
                return false;
            }*/
        }
        else 
            return false;
    }

    public static function getRole()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        
        $client = new Client("https://localhost:44301/api/Role");
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=$response->getBody();
        
        return $body;
    }
    
    public static function Register($username,$password)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
    
        $client = new Client("https://localhost:44301/api/Account/Register");
    
        $client->setMethod(Request::METHOD_POST);
        
        
        $params = 'app=mediador&Email=' . $username .'&Password=' . $password .'&ConfirmPassword=' .$password;
        
        
        $len = strlen($params);
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len 
        ));
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
        
        
        $response = $client->send();
    
        $body=Json::decode($response->getBody());
        
    
        return $body;
    }
    
    public static function Logout()
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        
        $_SESSION['username'] = null;
        $_SESSION['access_token'] = null;
    }
    
    public static function getPedidos(){
        if(!isset($_SESSION)){
            session_start();
        }
        
        $client = new Client('https://' .$_SESSION['server'] ."/pedidos");
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=$response->getBody();
        
        return $body;
        
        
    }
    
    public static function getValues()
    {
    if(!isset($_SESSION)){
                        session_start();
                    }

        $client = new Client('https://' .$_SESSION['server'] .self::URL_Values);
        
        $client->setMethod(Request::METHOD_GET);

        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=$response->getBody();
        
        return $body;
    }
    
    public static function getAllAnuncio($anuncio)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client("https://localhost:44301/api/".$anuncio);
        $client->setMethod(Request::METHOD_GET);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
    
        $client->setOptions(['sslverifypeer' => false]);
        $response = $client->send();
        return Json::decode($response->getBody(), true);
    
    }
    public static function get3DImages($id_imovel){
        if(!isset($_SESSION)){
            session_start();
        }
        $client = new Client("https://" .$_SESSION['server'] ."/api/Fotos3d?imovelID=" . $id_imovel);
        $client->setMethod(Request::METHOD_GET);
        
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        return Json::decode($response->getBody(), true);
    }
    public static function get3DImagesID(){
        if(!isset($_SESSION)){
            session_start();
        }
        $client = new Client("https://" .$_SESSION['server'] ."/api/Fotos3d");
        $client->setMethod(Request::METHOD_GET);
        
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        return Json::decode($response->getBody(), true);
    }
    
    public static function getAnuncio($anuncio,$id)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client("https://" .$_SESSION['server'] ."/api/" . $anuncio . "/" . $id);
        $client->setMethod(Request::METHOD_GET);
    
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
    
    
        $client->setOptions(['sslverifypeer' => false]);
    
        $response = $client->send();
    
        return Json::decode($response->getBody(), true);    
    }
    
    
    public static function editAnuncio($id, $tipo, $data){
        if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'].'/api/'.$tipo.'/'.$id);
    
        $client->setMethod(Request::METHOD_PUT);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
             'Authorization'   => $bearer_token,
        ));        
        
        $encoded = Json::encode($data);
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($encoded);
    
        $response = $client->send();
        
        return true;
    }
    
    public static function validaAnuncio($id, $tipo, $data){
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'].'/api/'.$tipo.'/'.$id);
    
        $client->setMethod(Request::METHOD_PUT);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
            'Authorization'   => $bearer_token,
        ));
    
        $data['valido'] = 'true';
        $encoded = Json::encode($data);
        
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($encoded);
    
        $response = $client->send();
    
        return true;
    }
    
    public static function removeAnuncio($id, $tipo){
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'].'/api/'.$tipo.'/'.$id);
    
        $client->setMethod(Request::METHOD_DELETE);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
            'Authorization'   => $bearer_token,
        ));
    
    
        $client->setOptions(['sslverifypeer' => false]);
    
        $response = $client->send();
    
        return true;
    }
    
    public static function associarVendaCompra($idCorrespondencia)
    { 
        // get correspondencia da webapi
        $correspondencia = ImoServices::getCorrespondencia($idCorrespondencia);  
        
        $idVenda = $correspondencia['Anuncio_venda_ID'];
        $idCompra = $correspondencia['Anuncio_compra_ID'];
        $venda = ImoServices::getAnuncio("Anuncio_Venda", $idVenda);
        $compra = ImoServices::getAnuncio("Anuncio_Compra", $idCompra);
        
        // se a correspondencia estiver a true, por a 'false' e os respectivos ID da compra e venda a '0'
        if($correspondencia['aceita_correspondencia'] == "true"){
            $correspondencia['aceita_correspondencia'] = "false";
            $venda['AnuncioCompraID'] = 0;
            $compra['AnuncioVendaID'] = 0;
        }else{
            // senao por a 'true' e os respetivos ID da compra e venda
            $correspondencia['aceita_correspondencia'] = 'true';
            $venda['AnuncioCompraID']=$idCompra;
            $compra['AnuncioVendaID']=$idVenda;
        }
          
        ImoServices::editAnuncio($idVenda, "Anuncio_Venda", $venda);
        ImoServices::editAnuncio($idCompra, "Anuncio_Compra", $compra);
        ImoServices::editCorrespondencia($idCorrespondencia, $correspondencia);
        
        return true;
    }
    
    public static function getAllCorrespondencias(){
        if(!isset($_SESSION)){
            session_start();
        }
        $client = new Client("https://localhost:44301/api/Correspondencia");
        $client->setMethod(Request::METHOD_GET);
        
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));    
        $client->setOptions(['sslverifypeer' => false]);        
        $response = $client->send();
        
        return Json::decode($response->getBody(), true);  
    }
    
    public static function getCorrespondencia($id){
        if(!isset($_SESSION)){
            session_start();
        }
        $client = new Client("https://localhost:44301/api/Correspondencia" . "/" . $id);
        $client->setMethod(Request::METHOD_GET);             
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));   
        $client->setOptions(['sslverifypeer' => false]);        
        $response = $client->send();       
        return Json::decode($response->getBody(), true);        
    }
    
    public static function editCorrespondencia($id,$data){
        if(!isset($_SESSION)){
            session_start();
        }
        $client = new Client("https://localhost:44301/api/Correspondencia" . "/" . $id);
        
        $client->setMethod(Request::METHOD_PUT);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
            'Authorization'   => $bearer_token,
        ));
        
        $encoded = Json::encode($data);
        
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($encoded);
        
        $response = $client->send();
        
        return true;
    }
}

?>